use std::thread;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use clap::{Arg, Command};

struct Args {
    key: String,
    local: String,
    remote: String,
}
fn main() {
    let arguments: (String, String, String) = handle_cli_args();

    let args = Args {
        key: arguments.0.clone(),
        local: arguments.1.clone(),
        remote: arguments.2.clone(),
    };

    // repeatedly send new messages
    thread::spawn(move || {
        loop {
            let message = read_message_from_terminal();

            match TcpStream::connect(&args.remote) {
                Ok(mut stream) => {
                    let ciphertext = vigenere_encrypt(message, &args.key);
                    stream.write(ciphertext.as_bytes()).expect("Failed to send message to remote.");
                }
                Err(_) => {
                    println!("Failed to connect to remote address.");
                }
            }
        }
    });

    // connection is automatically closed when variable leaves scope
    let tcp = TcpListener::bind(&args.local).expect("Failed to bind to local address.");

    // listen for incoming messages
    for stream in tcp.incoming() {
        match stream {
            Ok(mut stream) => {
                let mut buff = String::new();
                stream.read_to_string(&mut buff).expect("Failed to read message.");
                let cleartext = vigenere_decrypt(buff, &arguments.0);
                println!("{}", cleartext);
            }
            Err(e) => {
                println!("Error receiving message: {}.", e);
            }
        }
    }
}

/// parse command line arguments
/// returns (key, local address, remote address)
fn handle_cli_args() -> (String, String, String) {
    let matches = Command::new("netmesg")
        .version("0.1.0")
        .author("Nathan Harmon")
        .about("A simple terminal messenger which uses the Vigenere cipher for encryption.")
        .arg(Arg::new("key")
            .short('k')
            .long("key")
            .help("Shared secret key for Vigenere cipher.")
            .takes_value(true)
            .required(true))
        .arg(Arg::new("local")
            .short('l')
            .long("local")
            .help("Local address and port to bind to.")
            .takes_value(true)
            .default_value("127.0.0.1:59823"))
        .arg(Arg::new("remote")
            .short('r')
            .long("remote")
            .help("Remote address and port to bind to.")
            .takes_value(true)
            .required(true))
        .get_matches();

    (matches.value_of("key").unwrap().to_string(),
    matches.value_of("local").unwrap().to_string(),
    matches.value_of("remote").unwrap().to_string())
}

fn read_message_from_terminal() -> String {
    let mut message = String::new();

    std::io::stdin().read_line(&mut message).expect("Failed to read terminal input.");

    message.replace('\n', "")
}

const MAP: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,.!?;:' ";

fn vigenere_encrypt(message: String, key: &String) -> String {
    let mut cipher_text: String = String::with_capacity(message.len());

    message.char_indices().for_each(|(i, c)| cipher_text.push(
            nth(MAP,
                (position(c) + // take the message
                position(nth(&key[..], i % key.len()))) // add by the key
                % MAP.len()) // handle overflow
            ));

    cipher_text
}

fn vigenere_decrypt(cipher_text: String, key: &String) -> String {
    let mut clear_text: String = String::with_capacity(cipher_text.len());

    cipher_text.char_indices().for_each(|(i, c)| clear_text.push(
            nth(MAP,
                (MAP.len() + // add the length of possible letters map to prevent underflow
                position(c) - // take the ciphertext
                position(nth(&key[..], i % key.len()))) // subtract by the key
                % MAP.len())
            ));

    clear_text
}

/// Helper function to get the nth letter in a str
fn nth(key: &str, n: usize) -> char {
    key.chars().nth(n).unwrap()
}

/// Helper function to find the index of the character in MAP
fn position(c: char) -> usize {
    MAP.chars().position(|x| x == c).unwrap() as usize
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vigenere_encrypt() {
        assert_eq!(vigenere_encrypt("hello world i am speaking".to_string(), &"test".to_string()), "AiDEHdOHKpvsBdsFswHxtoAGz".to_string());
    }

    #[test]
    fn test_vigenere_decrypt() {
            assert_eq!(vigenere_decrypt("AiDEHdOHKpvsBdsFswHxtoAGz".to_string(), &"test".to_string()), "hello world i am speaking");
    }
}
