# netmesg

A terminal TCP messenger program using the Vigenère cipher for message encryption.

## Usage

`$ netmesg --key <key> --remote <remote> --local <local>`

key: a shared secret text to perform encryption with

remote: IP address and port of the friend you want to message

local: IP address and port you want your client to be available at.

*Note: If the port is restricted by the OS, root privileges may be required. Depending on the network configuration, port forwarding may be required.*

Once you have run the program, simply type the message and press enter. Exit the program using Ctrl + C.

## Example

`$ netmesg --key "helloworld" --remote "192.168.1.5:5576" --local "192.168.1.3:3990"`

## License

Copyright (C) 2022 Nathan Harmon

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
